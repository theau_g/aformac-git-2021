# Projet Starter Osengo

Projet de démarrage en html avec utilisation de sass

## Installation 
Se place dans le dossier assets du projet puis exécuter la commande sass
Projet à lancer en node > 14.0.2

   ```bash
   $ cd assets
   $ sass --watch scss\index.scss css\style.css
   ```

Le contenu de l'index.scss (plus les fichiers importés) sont compliés dans le fichier style.css du dossier css.

**Si vous rencontrez cette erreur  (sur windows):**
*sass : Impossible de charger le fichier C:\Program Files\nodejs\sass.ps1, car l’exécution de scripts est désactivée sur ce système.*
Dans un powershell en adminstrateur, exécuter :
```bash
$ set-executionpolicy unrestricted
   ```

### Exemple code PHP

```php
<?php
	function getImc($weight, $height)
	{
		$height /= 100;
		return $weight / ($height*$height);
	}
?>
   ```

Fait par Théau Goncalves (et pas Théo)